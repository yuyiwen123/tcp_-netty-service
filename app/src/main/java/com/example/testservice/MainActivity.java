package com.example.testservice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.testservice.netty.NettyService;
import com.example.testservice.eventBusBean.ConnectStatusBean;
import com.haoxy.common.model.FileBean;
import com.haoxy.common.model.MessageBean;
import com.luck.picture.lib.tools.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    private Button btStart, btStop, btSend;
    private ImageView img, imgStatus;
    public static String folderName = Environment.getExternalStorageDirectory() + "/nettyImg";
    public static final int PORT = 10010;//端口号

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);
        btStart = findViewById(R.id.bt_start);
        btStop = findViewById(R.id.bt_stop);
        btSend = findViewById(R.id.bt_send);
        img = findViewById(R.id.img);
        imgStatus = findViewById(R.id.img_status);

        File folder = new File(folderName);
        //如果该文件夹不存在，则进行创建
        try {
            if (!folder.exists()) {
                folder.mkdirs();
            }
        } catch (Exception e) {
            Log.w(TAG, "mkdirs ERROR: " + e.getMessage());
        }

        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NettyService.getInstance().stop();
                NettyService.getInstance().start();
            }
        });
        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NettyService.getInstance().stop();
            }
        });

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NettyService instance = NettyService.getInstance();
                if (null == instance.channel || !instance.channel.isOpen()) {
                    ToastUtils.s(MainActivity.this, "客户端未连接");
                    return;
                }
                MessageBean messageBean = new MessageBean();
                messageBean.setMsg("服务端发送的消息");
                instance.sendMessage(messageBean);
            }
        });
    }

    /**
     * eventbus事件处理
     *
     * @param o
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRecevie(Object o) {
        if (null != o) {
            if (o instanceof MessageBean) {
                MessageBean messageBean = (MessageBean) o;
                Toast.makeText(MainActivity.this, messageBean.getMsg(), Toast.LENGTH_SHORT).show();
            } else if (o instanceof FileBean) {
                FileBean fileBean = (FileBean) o;
                Glide.with(MainActivity.this).load(fileBean.getBytes()).into(img);
            } else if (o instanceof ConnectStatusBean) {
                ConnectStatusBean connectStatusBean = (ConnectStatusBean) o;
                if ("1".equals(connectStatusBean.getStatus())) {
                    Glide.with(MainActivity.this).load(ContextCompat.getDrawable(MainActivity.this, R.mipmap.show_wifi)).into(imgStatus);
                } else {
                    Glide.with(MainActivity.this).load(ContextCompat.getDrawable(MainActivity.this, R.mipmap.no_wifi)).into(imgStatus);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        NettyService.getInstance().stop();
    }

}