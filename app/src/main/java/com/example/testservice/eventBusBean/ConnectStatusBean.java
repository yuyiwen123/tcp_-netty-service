package com.example.testservice.eventBusBean;

import java.io.Serializable;

/**
 * 标记是否连接成功的标记类
 */
public class ConnectStatusBean implements Serializable {
    private String status;//1，连接成功 2，连接失败

    public ConnectStatusBean() {
    }

    public ConnectStatusBean(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
