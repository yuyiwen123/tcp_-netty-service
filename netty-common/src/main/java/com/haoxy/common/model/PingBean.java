package com.haoxy.common.model;

import java.io.Serializable;

public class PingBean implements Serializable {
    private String ping="SERVICE_PING";

    public String getPing() {
        return ping;
    }

    public void setPing(String ping) {
        this.ping = ping;
    }
}
