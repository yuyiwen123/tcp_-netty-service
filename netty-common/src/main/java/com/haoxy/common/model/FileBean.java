package com.haoxy.common.model;

import java.io.Serializable;

public class FileBean implements Serializable {
    // 文件字节数组
    private byte[] bytes;
    private String fileName;

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
